MaximoPlus Base Template

### What is this repository for? ###

* Basic Template for MaximoPlus. Contains the MaximoPlus library and the minimum of styling, with no mobile layout. It should rarely be used by itself, it is a dependency for other templates. It may be useful for the quick testing and proof of concept

### Installation ###

bower install maximoplus-base-template




### Contact  ###

dusan@maximoplus.com