maximoplus.core.globalFunctions.global_login_function = function(err){
    window.location="login-general.html";
}


function reloadme(){
     g_grid.dispose();
     b_cont.dispose();
    maximoplus.core.setOffline(true);
    dinit();
}


function dinit(){

b_cont = new maximoplus.controls.AppContainer("po","po");
b_cont.setOfflineEnabled(true);


rs_cont=new maximoplus.controls.RelContainer(b_cont,"poline");
rs_cont.setOfflineEnabled(true);

grs_grid = new maximoplus.controls.Grid(rs_cont,["ponum","polinenum","itemnum","storeloc"],10);
g_grid = new maximoplus.controls.Grid(b_cont,["_selected","ponum","status",  "statusdate", "description","purchaseagent.worksite","shipvia"], 10);
g_grid.addMeta("status","hasQbeLookup",true);
g_grid.addMeta("status","listColumns",["value", "description"]);



//wf = new maximoplus.controls.WorkflowControl(b, "POSTATUS");
//wf.routeWf();

b_cont.renderDeferred(document.getElementById("div1"));
g_grid.renderDeferred(document.getElementById("div2"));
//bbs.renderDeferred(document.getElementById("div1"));
rs_cont.renderDeferred(document.getElementById("div1"));
grs_grid.renderDeferred(document.getElementById("div2"));
s_sec = new maximoplus.controls.Section(b_cont,["ponum","status","statusdate","description"]);
s2_sec= new maximoplus.controls.Section(b_cont,["shipvia"]);
s2_sec.addMeta("shipvia", "hasLookup", true);
s_sec.addMeta("description","hasLookup",true);
s_sec.addMeta("statusdate","hasLookup",true);

    
s_sec.renderDeferred(document.getElementById("div2"));
s2_sec.renderDeferred(document.getElementById("div2"));

g_grid.initData();
//grs.initData();
s_sec.initData();
    
r_cont=new maximoplus.controls.RelContainer(b_cont,"doclinks");
r_cont.setOfflineEnabled(true);
rg_grid=new maximoplus.controls.Grid(r_cont,["document","ownertable","docinfo.description"],20);
rg_grid.addVirtualColumn(8,"delaction",{
  css:"delcss",
  action:function(field){
    if (field.getParent().getFieldLocalValue("deleted")){
      field.undeleteControlRow();
    }else{
      field.deleteControlRow();
    }
  },
  callback:function(field){
    if (field.getParent().getFieldLocalValue("deleted")){
      //this doesn't look logical, but the callback is called immediately after the action, before the data has been updated in local
      field.getElement().innerHTML="D";
    }else{
      field.getElement().innerHTML="U";
    }
  }});
rg_grid.addFieldDomTransform("delaction",
                       [function(field){
                          var row=field.getParent();
                          if (row.isDeleted()){
                            field.getElement().innerHTML="U";
                            }else{
                              field.getElement().innerHTML="D";
                              }
                        }]);
rg_grid.renderDeferred();
rg_grid.initData();




}

window.onload=dinit;

